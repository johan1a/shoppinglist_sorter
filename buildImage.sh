#!/bin/sh -e
TAG=$(git rev-parse HEAD)
docker build -t johan1a/shoppinglist-sorter:${TAG} .
docker build -t johan1a/shoppinglist-sorter:latest .
