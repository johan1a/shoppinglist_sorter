#!/bin/sh -e
TAG=$(git rev-parse HEAD)
docker push johan1a/shoppinglist-sorter:${TAG}
docker push johan1a/shoppinglist-sorter:latest
