#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from difflib import SequenceMatcher
import re

could_not_sort = sys.maxsize
threshold = 0.55


def get_similarity(product, reference_product):
    return SequenceMatcher(None, product, reference_product).ratio()


# Try to filter things like additional instructions or quantities
def filter_product(product):
    paren_regex = r'\(.*?\)'
    result = re.sub(paren_regex, '', product)

    trailing_paren_regex = '\\(.*$'
    result = re.sub(trailing_paren_regex, '', result)

    trailing_plus_regex = '\\+.*$'
    result = re.sub(trailing_plus_regex, '', result)

    quantity_regex = '^[0-9,. ⅓½¾⅐⅔]+'
    result = re.sub(quantity_regex, '', result)

    unit_regex = '^(dl|l|liter|-g|g|gram|grams|kilo|kilos|kg|cup|cups|quart|quarts) '
    result = re.sub(unit_regex, '', result)

    result = result.replace("to garnish", "")
    result = result.replace("chopped", "")
    result = result.replace("pitted", "")
    result = result.replace("drained", "")
    result = result.replace("sliced", "")
    result = result.replace("peeled", "")
    result = result.replace("finely", "")
    result = result.replace("tsp", "")
    result = result.replace("teaspoon", "")
    result = result.replace("tbsp", "")
    result = result.replace("tablespoon", "")
    result = result.replace("cup", "")
    result = result.replace("and", "")
    return result


def order_from_reference(reference_order, product):

    best = 0
    best_product = ""
    sorted_index = len(reference_order)

    filtered_product = filter_product(product)

    for i in range(0, len(reference_order)):
        reference_product = reference_order[i].lower()
        similarity = get_similarity(filtered_product, reference_product)
        if similarity > best:
            best = similarity
            sorted_index = i
            best_product = reference_order[i]
    return {
        'sorted_index': sorted_index,
        'similarity': best,
        'matched_product': best_product
    }


def format_line(line, debug):
    text = line[0]
    if debug:
        return text+ " (" + line[1]['matched_product'].strip() + ", " + str(line[1]['similarity']) + ")"
    else:
        return text


ignored_lines = ["", "# Could not be sorted:"]

def sort_by_reference(reference, input, debug=False):
    line_and_index = [(line.strip(), order_from_reference(reference, line))
                      for line in input]
    line_and_index = sorted(line_and_index, key=lambda x: x[1]['sorted_index'])
    result = []
    for line in line_and_index:
        similarity = line[1]['similarity']

        if similarity >= threshold and line[0] not in ignored_lines:
            result.append(line)

    first_unsorted = True
    for line in line_and_index:
        similarity = line[1]['similarity']

        if similarity < threshold:
            text = line[0]
            if text not in ignored_lines:
                if first_unsorted:
                    result.append(["\n# Could not be sorted:",
                                   {
                                       'matched_product': "",
                                       'sorted_index': could_not_sort,
                                       'similarity': -1
                                   }])
                    first_unsorted = False
                result.append(line)

    return [format_line(line, debug) for line in result]


if __name__ == "__main__":
    reference_file = sys.argv[1]
    input_file = sys.argv[2]
    if len(sys.argv) > 3 and sys.argv[3] == "True":
        debug = True
    else:
        debug = False

    reference = []
    with open(reference_file, "r") as file:
        reference = file.readlines()
    input = []
    with open(input_file, "r") as file:
        input = file.readlines()

    print("\n".join(sort_by_reference(reference, input, debug)))
