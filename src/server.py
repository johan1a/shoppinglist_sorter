# -*- coding: utf-8 -*-
from logging.config import dictConfig
from flask import Flask, render_template, request, make_response
from sorter import sort_by_reference
import json

app = Flask(__name__)

# voodoo to make logging work
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


@app.route("/shoppinglist/", methods=['GET'])
@app.route("/shoppinglist", methods=['GET'])
@app.route("/", methods=['GET'])
def index():
    shopId = request.args.get('shopId')
    if shopId is not None and shopId == "ica":
        reference_file = "reference.csv"
    else:
        reference_file = "default_reference.csv"

    with open(reference_file, "r") as file:
        referenceProducts = "".join(file.readlines())

    with open("default_shopping_list.csv", "r") as file:
        shoppingList = "".join(file.readlines())

    return render_template("index.html",
                           shoppingList=shoppingList,
                           referenceProducts=referenceProducts)


@app.route("/shoppinglist/sortShoppingList", methods=['POST'])
@app.route("/sortShoppingList", methods=['POST'])
def post():
    data = request.json
    shoppingList = data['shoppingList'].split('\n')
    referenceProducts = data['referenceProducts'].split('\n')
    sortedList = "\n".join(sort_by_reference(referenceProducts, shoppingList))
    app.logger.info("result: " + sortedList)
    return json.dumps({'sortedList': sortedList})


# Needed when running behind a reverse proxy.
# There is most likely a cleaner way to do this
@app.route("/shoppinglist/static/js/client.js", methods=['GET'])
def client_js():
    with open("src/static/js/client.js") as file:
        content = "".join(file.readlines())
    response = make_response(content)
    response.headers['Content-Type'] = 'application/javascript; charset=utf-8'
    return response


if __name__ == '__main__':
    app.run(debug=True)
