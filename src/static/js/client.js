$(function(){
  $('form').on('submit', function(event){
    var shoppingList = $('#inputShoppingList').val();
    var referenceProducts = $('#inputReferenceProducts').val();
    console.log("function");
    $.ajax({
      url: '/shoppinglist/sortShoppingList',
      data: JSON.stringify({
        shoppingList: $('#shoppingList').val(),
        referenceProducts: $('#referenceProducts').val(),
      }),
      type: 'POST',
      dataType: 'json',
      contentType: 'application/json; charset=UTF-8',
      success: function(response){
        console.log(response.sortedList);
        $('#shoppingList').val(response.sortedList);
      },
      error: function(error){
        console.log(error);
      }
    });
    event.preventDefault();
  });
});
