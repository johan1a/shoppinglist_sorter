FROM python:alpine3.12

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

ENV FLASK_APP=src/server.py
CMD ["flask", "run", "--host", "0.0.0.0"]
