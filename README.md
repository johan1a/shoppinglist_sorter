
# Installing

```
pipenv install
```

# Running

```
env FLASK_APP=src/server.py pipenv run flask run
```

# Running using docker
```
./buildImage.sh
docker run --rm -p 5000:5000 johan1a/shoppinglist-sorter:latest
```
